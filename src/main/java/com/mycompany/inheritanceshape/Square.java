/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritanceshape;

/**
 *
 * @author Aritsu
 */
public class Square extends Rectangle{
    public Square(double side){
        super(side,side);
        System.out.println("Created square");
    }
    public void print(){
        System.out.println("Shape: Square " + " side: " + super.height + " Area = " + calArea());
    }
    
}
