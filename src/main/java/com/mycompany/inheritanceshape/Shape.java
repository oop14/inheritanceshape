/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritanceshape;

/**
 *
 * @author Aritsu
 */
public class Shape {
    public Shape() {
        System.out.println("Shape created");
    }
    public double calArea(){
        System.out.println("Calculating....");
        return 0.00;
    }
    public void print(){
        System.out.println("Shape:..... " + "Area = " + calArea());
    }
    
}
