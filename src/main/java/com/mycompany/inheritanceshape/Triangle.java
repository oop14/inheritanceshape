/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritanceshape;


/**
 *
 * @author Aritsu
 */
public class Triangle extends Shape {
    private double base;
    private double height;
    
    public Triangle(double base,double height){
        super();
        System.out.println("Triangle created");
        this.base = base;
        this.height = height;
        
    }
    @Override
    public double calArea() {
        super.calArea();
        return 0.5*height*base;
    }
    
    @Override
    public void print(){
        System.out.println("Shape: Triangle " + " base: " + this.base +" height: " + this.height + " Area = " + calArea());
    }
}
