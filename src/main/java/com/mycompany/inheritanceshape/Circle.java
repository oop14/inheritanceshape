/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritanceshape;

/**
 *
 * @author Aritsu
 */
public class Circle extends Shape{
    private double r;
    static final double pi = 22.0/7;
    
    public Circle(double r){
        super();
        System.out.println("Circle created");
        this.r = r;
          
    } 
    @Override
    public double calArea() {
        super.calArea();
        return pi * r * r;
    }
    
    @Override
    public void print(){
        System.out.println("Shape: Circle " + " radius: " + this.r + " Area = " + calArea());
    }
}
