/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritanceshape;

/**
 *
 * @author Aritsu
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape = new Shape();
        shape.print();
        
        Circle circle1 = new Circle(3);
        circle1.print();
        Circle circle2 = new Circle(4);
        circle2.print();
        
        Triangle triangle1 = new Triangle(3,4);
        triangle1.print();
        
        Rectangle rectangle1 = new Rectangle(3,4);
        rectangle1.print();
        
        Square square1 = new Square(2);
        square1.print();
        
        System.out.println("-----------------------------Loop print All Shape !!!!--------------------------------");
        Shape[] shapes = {circle1,circle2,triangle1,rectangle1,square1};
        for(int i = 0;i<shapes.length;i++){
            shapes[i].print();
        }
        
    }
}
